package de.hochgebaut.simpleKitSelector;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (c) 2018 hochgebaut | Nico Scherer
 * created on 17.01.2018-15:12
 **/
public class KitSelector {

    private SimpleKitSelector plugin = SimpleKitSelector.getInstance();
    private Inventory inventory;
    private Player player;
    private Listener listener;

    public KitSelector() {
        listener = new Listener() {
            @EventHandler
            public void on(InventoryClickEvent event) {
                if (event.getClickedInventory() != null && event.getClickedInventory().equals(inventory)) {
                    event.setCancelled(true);
                    ItemStack itemStack = event.getCurrentItem();
                    plugin.getKits().stream().forEach(kit -> {
                        if (itemStack.getItemMeta().getDisplayName().equals(kit.getIcon().getItemMeta().getDisplayName())) {
                            player.getInventory().setContents(kit.getKitItems());
                            player.sendMessage("§aDu hast nun das Kit: §7" + kit.getName() + " §aausgerüstet.");
                            return;
                        }
                    });
                }
            }
        };
        Bukkit.getPluginManager().registerEvents(listener, plugin);
    }

    public void open(Player player) {
        this.player = player;
        this.inventory = Bukkit.createInventory(player, 9, "§6Wähle ein Kit aus");
        plugin.getKits().stream().forEach(kit -> this.inventory.addItem(kit.getIcon()));
        this.player.openInventory(this.inventory);
    }
}
