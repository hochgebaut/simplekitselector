package de.hochgebaut.simpleKitSelector;

import de.hochgebaut.simpleKitSelector.commands.KitCommand;
import de.hochgebaut.simpleKitSelector.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) 2018 hochgebaut | Nico Scherer
 * created on 17.01.2018-14:54
 **/

public class SimpleKitSelector extends JavaPlugin {

    public static SimpleKitSelector instance;
    private List<Kit> kits = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        loadKits();
        registerCommands();
    }

    private void loadKits() {
        kits.add(new Kit("Kit des Entdeckers", "Desch des Kit von 1 Entdecker", Material.EXP_BOTTLE, new ItemStack[]{new ItemBuilder(Material.DIAMOND).getItemStack(), new ItemBuilder(Material.EMERALD).getItemStack()}));
        kits.add(new Kit("Kit des Testers", "Desch des Kit von eins Teschter", Material.TNT, new ItemStack[]{new ItemBuilder(Material.DIAMOND_BOOTS).getItemStack(), new ItemBuilder(Material.BOW).getItemStack()}));
        kits.add(new Kit("Wetterkit", "Desch das Wetter", Material.GRASS, new ItemStack[]{new ItemBuilder(Material.RED_GLAZED_TERRACOTTA).getItemStack(), new ItemBuilder(Material.FEATHER).getItemStack()}));
        kits.add(new Kit("Uneingefallener Name", "Nicht eingefallen", Material.ENDER_PEARL, new ItemStack[]{new ItemBuilder(Material.GHAST_TEAR).getItemStack(), new ItemBuilder(Material.OBSERVER).getItemStack()}));
        kits.add(new Kit("Kit 5", "Das ist das 5. Kit", Material.DIAMOND_PICKAXE,  new ItemStack[]{new ItemBuilder(Material.LEATHER).getItemStack(), new ItemBuilder(Material.ACTIVATOR_RAIL).getItemStack()}));
        kits.add(new Kit("Kit 6", "Das sechste Kit", Material.FENCE, new ItemStack[]{new ItemBuilder(Material.ACACIA_FENCE).getItemStack(), new ItemBuilder(Material.BOOK_AND_QUILL).getItemStack()}));
        kits.add(new Kit("Kit 7", "Ja auch ein siebtes noch...", Material.BOW, new ItemStack[]{new ItemBuilder(Material.BAKED_POTATO).getItemStack(), new ItemBuilder(Material.RED_GLAZED_TERRACOTTA).getItemStack()}));
        kits.add(new Kit("Kit 8", "Ja wenn du auch das noch willst...", Material.BLAZE_ROD, new ItemStack[]{new ItemBuilder(Material.BIRCH_WOOD_STAIRS).getItemStack(), new ItemBuilder(Material.BARRIER).getItemStack()}));
        kits.add(new Kit("Kit 9", "Jetzt reichts aber auch mal!", Material.DIAMOND_AXE, new ItemStack[]{new ItemBuilder(Material.CHEST).getItemStack(), new ItemBuilder(Material.ACTIVATOR_RAIL).getItemStack()}));
    }

    private void registerCommands() {
        getCommand("kit").setExecutor(new KitCommand());
    }

    public List<Kit> getKits() {
        return kits;
    }

    public static SimpleKitSelector getInstance() {
        return instance;
    }
}
