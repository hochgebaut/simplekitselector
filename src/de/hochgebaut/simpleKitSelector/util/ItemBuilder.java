package de.hochgebaut.simpleKitSelector.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Copyright (c) 2018 hochgebaut | Nico Scherer
 * created on 17.01.2018-15:03
 **/

public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private String name;
    private List<String> lore;

    public ItemBuilder(Material material) {
        this.itemStack = new ItemStack(material);
    }

    public ItemBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemStack getItemStack() {
        this.itemMeta = this.itemStack.getItemMeta();
        this.itemMeta.setDisplayName(this.name);
        this.itemMeta.setLore(this.lore);
        this.itemStack.setItemMeta(getItemMeta());
        return itemStack;
    }

    public ItemMeta getItemMeta() {
        return itemMeta;
    }

}
