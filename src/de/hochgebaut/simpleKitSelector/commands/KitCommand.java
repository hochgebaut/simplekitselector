package de.hochgebaut.simpleKitSelector.commands;

import de.hochgebaut.simpleKitSelector.KitSelector;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (c) 2018 hochgebaut | Nico Scherer
 * created on 17.01.2018-14:56
 **/

public class KitCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("§cDu musst ein Spieler sein!");
            return true;
        }
        Player player = (Player) commandSender;
        KitSelector selector = new KitSelector();
        selector.open(player);
        return true;
    }
}
