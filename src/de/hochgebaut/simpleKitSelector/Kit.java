package de.hochgebaut.simpleKitSelector;

import com.mysql.fabric.xmlrpc.base.Array;
import de.hochgebaut.simpleKitSelector.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * Copyright (c) 2018 hochgebaut | Nico Scherer
 * created on 17.01.2018-14:58
 **/

public class Kit {

    private String name;
    private String description;
    private ItemStack icon;
    private ItemStack[] kitItems;

    public Kit(String name, String description, Material iconMaterial, ItemStack[] kitItems) {
        this.name = name;
        this.description = description;
        this.icon = new ItemBuilder(iconMaterial).setName(name).setLore(Arrays.asList(description)).getItemStack();
        this.kitItems = kitItems;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public ItemStack[] getKitItems() {
        return kitItems;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    public void setKitItems(ItemStack[] kitItems) {
        this.kitItems = kitItems;
    }
}
